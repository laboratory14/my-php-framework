<?php

namespace app\core;


final class Application {

    public static string $ROOT_DIR;

    public Router $router;

    public Request $request;

    public Response $response;

    public static Application $app;

    public function __construct(string $rootPath)
    {
        self::$app = $this;
        self::$ROOT_DIR = $rootPath;

        $this->request = new Request();
        $this->response = new Response();

        $this->router = new Router($this->request, $this->response);
    }

    public function run()
    {
        echo $this->router->resolve();
    }
}
