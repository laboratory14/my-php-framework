<?php

require_once __DIR__ . '/../vendor/autoload.php';

use app\controllers\SiteController;
use app\core\Application;


$app = new Application(dirname(__DIR__));

$app->router->get('/', 'home');

$app->router->get('/about', 'about');
$app->router->post('/about', [SiteController::class, 'handleContact']);

$app->run();
